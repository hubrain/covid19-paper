# Percentage of reported Covid-19 cases in Colombia: Estimating the true scale of the pandemic

[![Paper](https://img.shields.io/badge/Paper-medRxiv-blue)](https://www.medrxiv.org/content/10.1101/2020.12.30.20249052v1)

The code in this repository is used to reproduce the figures found at the paper shown above.
