import geopandas as gpd
import unidecode
import pandas as pd
import datetime
import os
from covidcolombia.constants import NAME_DEMO_DIST_COL, NAME_CFR_KOR, SUMMARY_NAME, NAME_TESTS_DB, INS_DEPTO_TO_TEST
from covidcolombia import Summary
from load_summary import get_summary
import matplotlib.pyplot as plt
import numpy as np
from covidcolombia.utils import concat_csv, parsing_string_regiones
from matplotlib.colors import Normalize, LogNorm
from matplotlib import cm

def nbi2ins(region):
    if region == "BOGOTÁ, D.C.":
        return "BOGOTA"
    elif region == "LA GUAJIRA":
        return "GUAJIRA"
    elif region == 'NORTE DE SANTANDER':
        return "NORTE SANTANDER"
    elif region == "VALLE DEL CAUCA":
        return "VALLE"
    elif region == 'ARCHIPIÉLAGO DE SAN ANDRÉS, PROVIDENCIA Y SANTA CATALINA':
        return "SAN ANDRES"
    elif region == "TOTAL NACIONAL":
        return "Colombia"
    elif region == "CÓRDOBA ":
        return "CORDOBA"
    elif region == 'NARIÑO':
        return "NARIÑO"
    else:
        return unidecode.unidecode(region)

def get_average_ages(dane_distribution_folder="../data/distribuciones_regiones"):
    df = concat_csv(dane_distribution_folder)
    df = df[["departamento", "EDAD", "HOMBRES_2018", "MUJERES_2018"]]
    regiones = df["departamento"].unique()
    data = {}
    for region in regiones:
        # Primer rango de edad
        df_region = df[df["departamento"] == region]
        df_region["pop_perc"] = df_region.HOMBRES_2018 + df_region.MUJERES_2018
        df_region = df_region[df_region.EDAD != "100+"]
        df_region.pop_perc = df_region.pop_perc/df_region.pop_perc.sum()
        df_region["edad_avg"] = df_region.EDAD.apply(lambda x: sum([int(y) for y in x.split('-')])/2)
        avg_edad = np.dot(df_region.edad_avg, df_region.pop_perc)
        data[parsing_string_regiones(df_region.departamento.iloc[0])] = avg_edad
    return pd.DataFrame(pd.Series(data)).reset_index().rename(columns={0: "mean_age", "index": "region"})

age_dist = get_average_ages()

# Fuente: https://es.wikipedia.org/wiki/Anexo:Departamentos_de_Colombia_por_superficie

area = pd.Series({
    'AMAZONAS': 109665,
    'ANTIOQUIA': 63612,
    'ARAUCA': 23818,
    'ARCHIPIELAGO DE SAN ANDRES': 52,
    'ATLANTICO': 3388,
    'BOGOTA': 1775,
    'BOLIVAR': 25978,
    'BOYACA': 23189,
    'CALDAS': 7888,
    'CAQUETA': 88965,
    'CASANARE': 44640,
    'CAUCA': 29308,
    'CESAR': 22905,
    'CHOCO': 46530,
    'CORDOBA': 25020,
    'CUNDINAMARCA': 22633,
    'GUAINIA': 72238,
    'GUAJIRA': 20848,
    'GUAVIARE': 53460,
    'HUILA': 19890,
    'MAGDALENA': 23188,
    'META': 85635,
    'NARIÑO': 33268,
    'NORTE SANTANDER': 21658,
    'PUTUMAYO': 24885,
    'QUINDIO': 1845,
    'RISARALDA': 4140,
    'SANTANDER': 30537,
    'SUCRE': 10917,
    'TOLIMA': 23562,
    'VALLE': 22140,
    'VAUPES': 54135,
    'VICHADA': 100242,
    })
area = pd.DataFrame(area).reset_index().rename(columns={0: "area", "index": "region"}).reset_index()


pob = pd.read_excel('../data/anexo-proyecciones-poblacion-departamental_Area2018-2050.xlsx', header=11, engine='openpyxl')
pob = pob[(pob["AÑO"] == 2018) & (pob["ÁREA GEOGRÁFICA"]=="Total")][["DPNOM", "Total"]]
pob = pob.rename(columns={"DPNOM": "region", "Total": "population"})
pob.region = pob.region.apply(lambda x: nbi2ins(x.upper()))


today = str(datetime.date.today())
today = "2020-12-28"
print("Date being used is", today)
summary_path = os.path.join('.', 'tmp', today, SUMMARY_NAME)
if os.path.isfile(summary_path):
    summary = get_summary(summary_path)
else:
    summary = get_summary(regions=None, date=today)
    
summary = summary[summary.rango == "(0-MAX_AGE)"]
summary = summary.merge(area, how="inner", on="region")
summary = summary.merge(pob, how="inner", on="region")
summary = summary.merge(age_dist, how="inner", on="region")
summary["pop_density"] = summary.population / summary.area
summary["log_pop_density"] = np.log(summary.pop_density)

states = gpd.read_file('../data/shapefile/Colombia_COVID19_Coronavirus_Departamento.shp')
states.NOMBRE_DPT = states.NOMBRE_DPT.apply(lambda x: nbi2ins(x))

states_urep = states.rename(columns={"NOMBRE_DPT":"region"}).merge(summary, on='region', how='left')
states_urep['coords'] = states_urep['geometry'].apply(lambda x: x.representative_point().coords[:])
states_urep['coords'] = [coords[0] for coords in states_urep['coords']]
states_urep = states_urep[states_urep.region != 'SAN ANDRES']
states_urep = states_urep.sort_values(by='region')
states_urep["number"] = [str(i + 1) for i in range(states_urep.shape[0])]


# Plot the map for some feature:
fig, axes = plt.subplots(ncols=3, figsize=(17, 7))
axes = axes.flatten()

feats = ["urep", "mean_age", "log_pop_density"]
names = ["Reporting percentage [%]", r"Mean Age [years]", r"Population Density [inhabitants/km${}^2$]"]
normalisers = [Normalize, Normalize, LogNorm]
cmap = 'PiYG'
for feat, ax, name, colormap_norm in zip(feats, axes, names, normalisers):
    print("Creating map for", feat)
    states_urep.plot(column=feat, legend=False, ax=ax, edgecolor='k', linewidth=0.1, cmap=cmap)
    states_urep.apply(lambda x: ax.annotate(text=x.number, xy=x.geometry.centroid.coords[0], ha='center', fontsize=7, fontweight='bold'),axis=1)
    feat = feat.replace("log_", "")
    mn = states_urep[feat].min()
    mx = states_urep[feat].max()
    norm = colormap_norm(vmin=mn, vmax=mx)
    n_cmap = cm.ScalarMappable(norm=norm, cmap=cmap)
    n_cmap.set_array([])
    colorbar = fig.colorbar(n_cmap, ax=ax, orientation='horizontal', aspect=25, shrink=0.8, pad=0)
    colorbar.ax.set_title(name)
    ax.axis('off')
plt.tight_layout()
plt.savefig(f"../doc/Art1/img/maps_age_popdensity_repperc.pdf", dpi=300, bbox_inches='tight')
plt.show()