"""
To run this code, make sure you have npm installed:
sudo apt install npm
Then:
npm install vega-lite vega-cli canvas
Now you can run this file!
"""

import os
import numpy as np
import datetime
from covidcolombia.utils import colombia_demo_distribution, korea_cfr, agerange2string
from covidcolombia import Region, Report
from covidcolombia.constants import NAME_DEMO_DIST_COL, NAME_CFR_KOR, AGE_RANGES
import altair as alt
from altair_saver import save


path = os.path.abspath('./tmp')
dane_path = os.path.join(path, "../../data/distribuciones_regiones")

# We will save the INS files in examples/
col_demo_path = os.path.join(path, NAME_DEMO_DIST_COL)
korea_cfr_path = os.path.join(path, NAME_CFR_KOR)
colombia_demo_distribution(dane_path, col_demo_path, 3.0)
korea_cfr(korea_cfr_path)

today = datetime.date.today()
report = Report(path, date=str(today))
ft = report.fit_distribution()
ft = np.mean(ft, axis=0)

# Initialise Region
colombia = Region(path, col_demo_path, korea_cfr_path, region=None, date=str(today), delay_distribution=ft)
df = colombia.get_curves(str(today))
df_all = df[df.rango == agerange2string(AGE_RANGES[-1])]

base = alt.Chart(df_all).encode(
    alt.X('date:T', axis=alt.Axis(title=None))
)

deaths = base.mark_area(
    line={'color':'darkred'},
    color=alt.Gradient(
        gradient='linear',
        stops=[
            alt.GradientStop(color='white', offset=0),
            alt.GradientStop(color='darkred', offset=1)
        ],
        x1=1,
        x2=1,
        y1=1,
        y2=0
    )
).encode(
    alt.Y('death_incidence:Q', axis=alt.Axis(title='Deaths'))
)

cases = base.mark_area(
    line={'color':'darkblue'},
    color=alt.Gradient(
        gradient='linear',
        stops=[
            alt.GradientStop(color='white', offset=0),
            alt.GradientStop(color='darkblue', offset=1)
        ],
        x1=1,
        x2=1,
        y1=1,
        y2=0
    )
).encode(
    alt.Y('case_incidence:Q', axis=alt.Axis(title='Cases'))
)

chart = alt.vconcat(cases, deaths).resolve_scale(
    x = 'shared'
)

save(chart, "../doc/Art1/img/death_case_incidence.pdf")