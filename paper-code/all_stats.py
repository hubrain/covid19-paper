import pandas as pd 
import numpy as np 
import pickle
from matplotlib import pyplot as plt
from matplotlib.font_manager import FontProperties
import os
from scipy import stats
from onset_to_death_fit import main as distribu_fit 


def table_Korea():
	r'''
	Function that generates the table with the CFR information
	from Korea. Generates a .tex file

	Returns
	----------
	Save .tex file with a latex table file in "../doc/Art1/tab/table_Korea.tex" 
	with CFR information from Korea

	'''
	#Read DataFrame with Korea data
	df = pd.read_csv("tmp/cfr_korea.csv")
	# discard the index column
	df = df.iloc[:,1:]
	# correct format for the "rango"
	df['rango'] = df['rango'].str.strip('()').str.replace(',', ' -')

	# change symbol for the last age-range
	df.loc[5,'rango'] = ('$\\geq$80')

	# Generates and write the table in LaTex format
	with open("../doc/Art1/tab/table_Korea.tex","w+") as f:
		f.write("\\begin{tabular}{cl}\n")
		f.write("Age Groups (years) &  \multicolumn{1}{c}{Case Fatality Ratio (95\\% CI)}\\\\\n")
		f.write("\\hline\n")
		for i, row in df.iterrows():
			f.write("{0} & {1:.2f}\\,\\%\\, ({2:.2f}-{3:.2f}) \\\\\n".format(row['rango'], row['cfr'], row['inf'], row['sup']))
		f.write("\\end{tabular}")
		f.close()

def table_underreport(df):
	r'''
	Function that generates the table with the percentage of report
	in Colombia and its regions. Generates a .tex file

	Attributes
	----------
	df: pandas.DataFrame
		Summary data with all calculates from Colombia

	Returns
	----------
	Save .tex file with a latex table file in "../doc/Art1/tab/table_underreport.tex"
	with the percentage of report in Colombia and its regions
	'''
	df_under = df[df['rango'] == '(0-MAX_AGE)']
	df_under = df_under.drop(['rango','nCFR'],axis=1)

	# Generates and write the table in LaTex format
	regiones = list(df_under['region'].sort_values().unique())
	regiones.remove("Colombia")
	region = df_under[df_under['region'] == 'Colombia'].iloc[0]

	renglon = ['Colombia']
	renglon.append("{0:.0f}\\,\\% ({1:.0f}-{2:.0f})".format(region['urep'], region['urep_inf'], region['urep_sup']))
	renglon.append("{0:.1f}\\,\\% ({1:.1f}-{2:.1f})".format(region['cCFR'], region['cCFR_inf'], region['cCFR_sup']))
	renglon.append("{0:.1f}\\,\\% ({1:.1f}-{2:.1f})".format(region['blCFR'], region['blCFR_inf'], region['blCFR_sup']))
	renglon.append("{0:.0f}".format(region['cases']))
	renglon.append("{0:.0f}".format(region['deaths']))
	with open("../doc/Art1/tab/table_underreport.tex","w+") as f:
		f.write("\\begin{tabular}{llrrrr}\n")
		f.write(" & \multicolumn{1}{l}{Percentage} & & & & \\\\\n")
		f.write(" & \multicolumn{1}{l}{of cases reported} & \multicolumn{1}{c}{cCFR} & \multicolumn{1}{c}{blCFR} & \multicolumn{1}{c}{Total cases} & \multicolumn{1}{c}{Total deaths}\\\\\n")
		f.write("\\hline\n")
		f.write(" & ".join(renglon) + "\\\\\n")
		#f.write("\\hline\n")
		#f.write("\\multicolumn{4}{l}{\\textbf{Regions}}\\\\\n")
		f.write("\\hline\n")
		for reg in regiones:
			region = df_under[df_under['region'] == reg].iloc[0]
			renglon = [reg]
			renglon.append("{0:.0f}\\,\\% ({1:.0f}-{2:.0f})".format(region['urep'], region['urep_inf'], region['urep_sup']))
			renglon.append("{0:.1f}\\,\\% ({1:.1f}-{2:.1f})".format(region['cCFR'], region['cCFR_inf'], region['cCFR_sup']))
			renglon.append("{0:.1f}\\,\\% ({1:.1f}-{2:.1f})".format(region['blCFR'], region['blCFR_inf'], region['blCFR_sup']))
			renglon.append("{0:.0f}".format(region['cases']))
			renglon.append("{0:.0f}".format(region['deaths']))
			f.write(" & ".join(renglon) + "\\\\\n")
		f.write("\\end{tabular}")
		f.close()

def table_underreport_ages(df):
	r'''
	Function that generates the table with the age-stratified percentage of report
	in Colombia and its regions and their base line CFR. Generates a .tex file.

	Attributes
	----------
	df: pandas.DataFrame
		Summary data with all calculates from Colombia

	Returns
	----------
	Save .tex file with a latex table file in "../doc/Art1/tab/table_underreport.tex"
	with age-stratified percentage of report in Colombia and its regions and their base
	line CFR
	'''
	# Generates and write the table in LaTex format
	regiones = list(df['region'].sort_values().unique())
	regiones.remove("Colombia")
	with open("../doc/Art1/tab/table_underreport_ages.tex","w+") as f:
		f.write("\\begin{tabular}{lllllll}\n")
		f.write("\\multicolumn{7}{c}{Percentange of cases reported by age group (years)}\\\\\n")
		f.write("& \multicolumn{1}{c}{0-39} & \multicolumn{1}{c}{40-49}& \multicolumn{1}{c}{50-59}& \multicolumn{1}{c}{60-69}& \multicolumn{1}{c}{70-79}&\multicolumn{1}{c}{$\geq$ 80}\\\\\n")
		f.write("\\hline\n")
		region = df[df['region'] == 'Colombia'].iloc[:6]
		renglon = ['Colombia']
		for i, row in region.iterrows():
			renglon.append("{0:.0f}\\,\\% ({1:.0f}-{2:.0f})".format(row['urep'], row['urep_inf'], row['urep_sup']))
		f.write(" & ".join(renglon) + "\\\\\n")
		#f.write("\\hline\n")
		#f.write("\\multicolumn{7}{l}{\\textbf{Regions}}\\\\\n")
		f.write("\\hline\n")
		for reg in regiones:
			region = df[df['region'] == reg]
			region = region.iloc[:6]
			renglon = [reg]
			for i, row in region.iterrows():
				renglon.append("{0:.0f}\\,\\% ({1:.0f}-{2:.0f})".format(row['urep'], row['urep_inf'], row['urep_sup']))
			f.write(" & ".join(renglon) + "\\\\\n")
		f.write("\\end{tabular}")
		f.close()

def main(date):
	r'''
	Funtion to calculate all stats to the Art1

	Attributes
	----------
	date: str
		Date for choose the data with formar 'yy-mm-dd'
	Returns
	----------

	'''
	#Table with CFR Korea data	
	table_Korea()
	#load data 
	df = pd.read_csv("tmp/{0}/summary.csv".format(date))

	#Underreporting table for all regions 
	table_underreport(df)

	#Underrrporting table for all age ranges and all regions
	table_underreport_ages(df)

	#it does all distributions stats for the paper
	distribu_fit()

if __name__ == "__main__":
	main("2020-12-28")






