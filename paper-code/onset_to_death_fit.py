from covidcolombia import Report
from covidcolombia.utils import log_lognormal_pdf, log_gamma_pdf, log_weibull_pdf
import numpy as np
import datetime
import os
from tqdm import tqdm
from scipy.special import gamma as Gamma
import matplotlib.pyplot as plt
import seaborn as sns
from scipy.integrate import quad
from scipy.stats import gaussian_kde

def rgb_to_float(r, g, b):
        return r / 255, g / 255, b / 255

def plot_distribution(name, data, probs, pal,xrange=np.linspace(0, 70, 1000), alpha=0.05):
    _fig, ax = plt.subplots()
    plt.title(name)
    ax.hist(
        data,
        bins=np.arange(0, len(np.unique(data))),
        alpha=0.5,
        density=True,
        color=pal[1],
    )
    for pdf in probs:
        ax.plot(xrange, pdf, color=pal[0], alpha=alpha)

    ax.set_xlabel('Onset to death (days)')
    ax.set_ylabel('Probability density')

    plt.xlim([0, 70])
    plt.savefig(
        f"../doc/Art1/img/{name}_onset_to_death.pdf",
        dpi=300,
        transparent=True,
        bbox_inches="tight",
    )
    plt.show()
    plt.clf()

def obtain_data(report,
    name="lognormal",
    param1="mus",
    param2="sigmas",
    mean_function=lambda m, s: np.exp(m + 0.5 * s ** 2),
    std_function=lambda m, s: np.sqrt(np.exp(2 * m + 2 * s ** 2)),
    xrange=np.linspace(0, 70, 1000),
    log_prob_func=log_lognormal_pdf,
):
    params, data = report.fit_distribution(
        dist=f"pymc3:{name}", probs_mode="direct", returnData=True
    )
    params1 = params[param1]
    params2 = params[param2]

    probs = []
    log_probs = []
    means = []
    stds = []
    for p1, p2 in tqdm(zip(params1, params2), total=params1.size):
        log_probs.append(log_prob_func(data, p1, p2))
        probs.append(np.exp(log_prob_func(xrange, p1, p2)))
        means.append(mean_function(p1, p2))
        stds.append(std_function(p1, p2))

    aics = []
    for logp in log_probs:
        logLikelikehood = np.sum(logp)
        k = 2
        aic = 2 * k - 2 * logLikelikehood
        aics.append(aic)
    aics = np.asarray(aics)

    return aics, data, probs, means, stds

def obtain_data_kde(report, xrange=np.linspace(0, 70, 1000)):
    params, data = report.fit_distribution(dist='kde', probs_mode="direct", returnData=True)
    kernel = gaussian_kde(data)
    probs = [kernel(xrange)]
    log_probs = [np.log(kernel(data))]
    means = [quad(lambda x: x*kernel(x)[0], -50, 400)[0]]
    stds = [np.sqrt(quad(lambda x: (x - means[0])**2 * kernel(x)[0], -50, 400))]
    aics = np.asarray([-2*np.sum(log_probs[0])])

    return aics, data, probs, means, stds
    

def main():
    today = str(datetime.date.today())
    path = os.path.abspath(os.path.dirname(__file__))
    report = Report(
        os.path.join(path, "tmp"), date=today, region="Colombia", age_range=None
    )

    aic_lognormal, data_lognormal, probs_lognormal, mean_lognormal, std_lognormal = obtain_data(
        report,
        name="lognormal",
        param1="mus",
        param2="sigmas",
        mean_function=lambda μ, σ: np.exp(μ + 0.5 * σ ** 2),
        std_function=lambda μ, σ: np.sqrt(np.exp(2 * μ + 2 * σ ** 2)),
        log_prob_func=log_lognormal_pdf)

    aic_gamma, data_gamma, probs_gamma, mean_gamma, std_gamma = obtain_data(
        report,
        name="gamma",
        param1="alphas",
        param2="betas",
        mean_function=lambda α, β: α / β,
        std_function=lambda α, β: np.sqrt(α / β ** 2),
        log_prob_func=log_gamma_pdf)

    mu_weibull = lambda α, β: β * Gamma(1 + 1 / α)
    aic_weibull, data_weibull, probs_weibull, mean_weibull, std_weibull = obtain_data(
        report,
        name="weibull",
        param1="alphas",
        param2="betas",
        mean_function=lambda α, β: mu_weibull(α, β),
        std_function=lambda α, β: np.sqrt(β ** 2 * Gamma(1 + 2 / α - mu_weibull(α, β) ** 2 / β ** 2)),
        log_prob_func=log_weibull_pdf)

    aic_kde, data_kde, probs_kde, mean_kde, std_kde = obtain_data_kde(report)
    # Plot the results:

    pal = [rgb_to_float(3, 4, 94), rgb_to_float(0, 119, 182), rgb_to_float(0, 180, 216)]

    # First let's see AIC distributions:

    ax = sns.kdeplot(aic_gamma, shade=True, color=pal[0], label="Gamma", gridsize=1000)
    sns.kdeplot(
        aic_weibull, shade=True, color=pal[1], label="Weibull", ax=ax, gridsize=1000
    )
    sns.kdeplot(
        aic_lognormal, shade=True, color=pal[2], label="Lognormal", ax=ax, gridsize=1000
    )
    ax.set_xlabel("Akaike Information Criterium")
    plt.legend()
    plt.savefig(
        "../doc/Art1/img/aic_dists_onset_to_death.pdf",
        dpi=300,
        transparent=True,
        bbox_inches="tight",
    )
    plt.show()
    plt.clf()

    # Now the distributions:

    plot_distribution("Gamma", data_gamma, probs_gamma,pal)
    plot_distribution("Lognormal", data_lognormal, probs_lognormal,pal)
    plot_distribution("Weibull", data_weibull, probs_weibull,pal)
    plot_distribution("KDE", data_kde, probs_kde,pal, alpha=1)

    # Compute the weights:

    mean_aic_gamma = np.mean(aic_gamma)
    mean_aic_weibull = np.mean(aic_weibull)
    mean_aic_lognormal = np.mean(aic_lognormal)
    mean_aic_kde = np.mean(aic_kde)
    aics = np.array([mean_aic_gamma, mean_aic_weibull, mean_aic_lognormal, mean_aic_kde])

    weight_weibull = np.exp(-0.5 * (mean_aic_weibull - min(aics))) / np.sum(
        np.exp(-0.5 * (aics - min(aics)))
    )

    weight_lognormal = np.exp(-0.5 * (mean_aic_lognormal - min(aics))) / np.sum(
        np.exp(-0.5 * (aics - min(aics)))
    )

    weight_gamma = np.exp(-0.5 * (mean_aic_gamma - min(aics))) / np.sum(
        np.exp(-0.5 * (aics - min(aics)))
    )

    weight_kde = np.exp(-0.5 * (mean_aic_kde - min(aics))) / np.sum(
        np.exp(-0.5 * (aics - min(aics)))
    )

    # Create LaTeX table:

    with open("../doc/Art1/tab/table_AIC.tex", "w") as f:

        f.write("\\begin{tabular}{lllll}\n")
        f.write("&  Gamma & Weibull & Lognormal & KDE\\\\\n")
        f.write("\\hline\n")
        f.write("Mean (days) & ")
        # Gamma
        mean_ = np.mean(mean_gamma)
        lo = np.percentile(mean_gamma, 2.25)
        hi = np.percentile(mean_gamma, 97.75)
        f.write(f"{mean_:.1f} ({lo:.1f}-{hi:.1f}) & ")
        # Weibull
        mean_ = np.mean(mean_weibull)
        lo = np.percentile(mean_weibull, 2.25)
        hi = np.percentile(mean_weibull, 97.75)
        f.write(f"{mean_:.1f} ({lo:.1f}-{hi:.1f}) & ")
        # Lognormal
        mean_ = np.mean(mean_lognormal)
        lo = np.percentile(mean_lognormal, 2.25)
        hi = np.percentile(mean_lognormal, 97.75)
        f.write(f"{mean_:.1f} ({lo:.1f}-{hi:.1f}) & ")
        # KDE
        mean_ = np.mean(mean_kde)
        f.write(f"{mean_:.1f} \\\\\n")
        

        f.write("SD (days) & ")
        # Gamma
        mean_ = np.mean(std_gamma)
        lo = np.percentile(std_gamma, 2.25)
        hi = np.percentile(std_gamma, 97.75)
        f.write(f"{mean_:.1f} ({lo:.1f}-{hi:.1f}) & ")
        # Weibull
        mean_ = np.mean(std_weibull)
        lo = np.percentile(std_weibull, 2.25)
        hi = np.percentile(std_weibull, 97.75)
        f.write(f"{mean_:.1f} ({lo:.1f}-{hi:.1f}) & ")
        # Lognormal
        mean_ = np.mean(std_lognormal)
        lo = np.percentile(std_lognormal, 2.25)
        hi = np.percentile(std_lognormal, 97.75)
        f.write(f"{mean_:.1f} ({lo:.1f}-{hi:.1f}) & ")
        # KDE
        mean_ = np.mean(std_kde)
        f.write(f"{mean_:.1f} \\\\\n")

        f.write("AIC ($\\times 10^3$) & ")
        mean_aic_gamma = mean_aic_gamma * 1e-3
        mean_aic_weibull = mean_aic_weibull * 1e-3
        mean_aic_lognormal = mean_aic_lognormal * 1e-3
        mean_aic_kde = mean_aic_kde * 1e-3
        f.write(
            f"{mean_aic_gamma:.1f} & {mean_aic_weibull:.1f} & {mean_aic_lognormal:.1f} & {mean_aic_kde:.1f}\\\\\n"
        )

        f.write("Weight & ")
        f.write(
            f"{weight_gamma:.1f} & {weight_weibull:.1f} & {weight_lognormal:.1f} & {weight_kde:.1f} \\\\\n"
        )
        f.write("\\end{tabular}")
