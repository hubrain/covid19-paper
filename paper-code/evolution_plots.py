import matplotlib.pyplot as plt
from covidcolombia import Summary
import datetime
import os
from covidcolombia.constants import NAME_DEMO_DIST_COL, NAME_CFR_KOR, EVOLUTION_NAME, AGE_RANGES
from covidcolombia.utils import colombia_demo_distribution, korea_cfr, agerange2string
import pandas as pd
import numpy as np
from scipy.signal import savgol_filter
from scipy.interpolate import make_interp_spline
import matplotlib.dates as mdates

def get_evolution(evolution_path=None, regions=["Colombia"], date=str(datetime.date.today())):
    """
    Computes the evolution of a series of regions.

    Parameters
    ----------
    evolution_path: str or None, optional
        An absolute path leading to a dataframe obtained with 
        `Summary.reporting_percentage_evolution`. If None, a
        dataframe is created.
    regions: List[str], optional
        List of regions to get the evolution from.
    date: str, optional
        A date in format "YYYY-MM-DD".
    """
    if evolution_path is None:
        path = os.path.abspath(os.path.dirname(__file__))
        dane_path = os.path.join(path, "../data/distribuciones_regiones")
        path = os.path.join(path, 'tmp')
        # We will save the INS files in a tmp/ folder

        try:
            os.mkdir('tmp')
        except:
            pass

        col_demo_path = os.path.join(path, NAME_DEMO_DIST_COL)
        korea_cfr_path = os.path.join(path, NAME_CFR_KOR)
        colombia_demo_distribution(dane_path, col_demo_path, 3.0)
        korea_cfr(korea_cfr_path)
        summary = Summary(path, date, col_demo_path, korea_cfr_path, regions=regions)
        df = summary.reporting_percentage_evolution("2020-05-01", date, "ignorant", min_deaths=40, dist='kde')
        return df
    else:
        return pd.read_csv(evolution_path)

def smooth_line(date_range, signal, poly_order=3, window=151):
    """
    """
    # TODO: write documentation
    start = pd.Timestamp(min(date_range))
    end = pd.Timestamp(max(date_range))
    T = np.asarray([date.value for date in date_range])
    t = np.linspace(start.value, end.value, 2000)
    date_range_fine = pd.to_datetime(t)
    spline = make_interp_spline(T, signal, k=poly_order)
    sp_signal = spline(t)

    smooth_signal = savgol_filter(sp_signal, window, poly_order)
    return date_range_fine, smooth_signal

def rgb_to_float(r, g, b):
    return r / 255, g / 255, b / 255

def plot_reporting_percentage(region_name, date_range, inf, sup, plot_dots=False, ax=None, verbose=False):
    """
    """
    #TODO: write documentation
    if ax is None:
        _fig, ax = plt.subplots(figsize=plt.figaspect(1 / 1.61803))
    locator = mdates.AutoDateLocator(minticks=3, maxticks=6)
    formatter = mdates.ConciseDateFormatter(locator, show_offset=False)
    ax.xaxis.set_major_locator(locator)
    ax.xaxis.set_major_formatter(formatter)
    ax.tick_params(labelsize=16)
    dt, inf_smooth = smooth_line(date_range, inf)
    dt, sup_smooth = smooth_line(date_range, sup)
    ax.fill_between(dt, inf_smooth, sup_smooth, color=rgb_to_float(3, 4, 94), alpha=0.4)
    ax.set_title(region_name, fontsize=18)
    if max(sup_smooth) >= 100 and min(inf_smooth) <= 0:
        ax.set_ylim(0, 100)
    elif max(sup_smooth) >= 100:
        ax.set_ylim(None, 100)
    elif min(inf_smooth) <= 0:
        ax.set_ylim(0, None)
    if plot_dots:
        ax.plot(date_range, inf, 'ko')
        ax.plot(date_range, sup, 'ko')
    if verbose:
        plt.show()
        plt.clf()


if __name__ == "__main__":
    today = str(datetime.date.today())
    evolution_path = os.path.join('.', 'tmp', today, EVOLUTION_NAME)
    if os.path.isfile(evolution_path):
        evolution = get_evolution(evolution_path)
    else:
        #evolution = get_evolution(regions=["Amazonas", "Bogotá D.C."], date=today)
        evolution = get_evolution(regions=None, date=today)

    all_ages = agerange2string(AGE_RANGES[-1])
    length = len(evolution.region.unique())
    ncols = 5
    nrows = np.ceil(length / ncols)
    fig, axs = plt.subplots(ncols=ncols, nrows=int(nrows),figsize=(3.2*ncols,2*nrows))
    fig.text(0.5, 0.0, 'Dates', ha='center',fontsize=16)
    fig.text(0.0, 0.5, "% of cases reported", va='center', rotation='vertical',fontsize=16)
    axs = axs.flatten()
    
    colombia_done = False
    failed = 0
    for i, (region_name, region) in enumerate(evolution.groupby('region')):
        # Reorder regions for plotting
        if region_name == "Colombia":
            i = 0
            colombia_done = True
        else:
            if not colombia_done:
                i = i + 1
        region = region[region.rango == all_ages]
        dates = pd.to_datetime(region.day)
        inf = region.urep_inf.to_numpy()
        sup = region.urep_sup.to_numpy()
        try:
            plot_reporting_percentage(region_name, dates, inf, sup, plot_dots=False, verbose=False, ax=axs[i-failed])
        except:
            print(region_name, inf.shape)
            failed += 1
    
    plt.tight_layout()
    plt.savefig("../doc/Art1/img/evolution_truncated/under_reporting_evolution_panel_koopman.pdf", dpi=300, bbox_inches='tight', transparent=True)
    plt.show()
