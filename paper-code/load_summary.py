import os
import datetime
from covidcolombia.utils import colombia_demo_distribution, korea_cfr
from covidcolombia.constants import NAME_DEMO_DIST_COL, NAME_CFR_KOR, SUMMARY_NAME
from covidcolombia import Summary
import pandas as pd

def get_summary(summary_path=None, regions=["Colombia"], date=str(datetime.date.today())):
    """
    Computes the summary for a series of regions.

    Parameters
    ----------
    summary_path: str or None, optional
        An absolute path leading to a dataframe obtained with 
        `Summary.summarise`. If None, a dataframe is created.
    regions: List[str], optional
        List of regions to get the evolution from.
    date: str, optional
        A date in format "YYYY-MM-DD".
    """
    if summary_path is None:
        path = os.path.abspath(os.path.dirname(__file__))
        dane_path = os.path.join(path, "../data/distribuciones_regiones")
        path = os.path.join(path, 'tmp')
        # We will save the INS files in a tmp/ folder

        try:
            os.mkdir('tmp')
        except:
            pass

        col_demo_path = os.path.join(path, NAME_DEMO_DIST_COL)
        korea_cfr_path = os.path.join(path, NAME_CFR_KOR)
        colombia_demo_distribution(dane_path, col_demo_path, 3.0)
        korea_cfr(korea_cfr_path)
        summary = Summary(path, date, col_demo_path, korea_cfr_path, regions=regions)
        df = summary.summarise(date, dist='kde')
        return df
    else:
        return pd.read_csv(summary_path)

if __name__ == "__main__":
    today = str(datetime.date.today())
    summary_path = os.path.join('.', 'tmp', today, SUMMARY_NAME)
    if os.path.isfile(summary_path):
        summary = get_summary(summary_path)
    else:
        #summary = get_summary(regions=["Amazonas", "Bogotá D.C."], date=today)
        summary = get_summary(regions=None, date=today)